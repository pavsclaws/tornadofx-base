package model.models

data class Direction(val label: String,
                     val type: DirectionType,
                     val resultText: String) {

    // При отображении в ComboBox выполняется item.toString()
    override fun toString(): String {
        return this.label
    }
}

enum class DirectionType { UP, DOWN, LEFT, RIGHT }