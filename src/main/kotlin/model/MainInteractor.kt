package model

import javafx.collections.FXCollections
import model.models.Direction
import model.models.DirectionType

class MainInteractor {

    fun getDirections() = FXCollections.observableArrayList(
            Direction(
                    label = "↑",
                    type = DirectionType.UP,
                    resultText = "Вверх"
            ),
            Direction(
                    label = "→",
                    type = DirectionType.RIGHT,
                    resultText = "Вправо"
            ),
            Direction(
                    label = "↓",
                    type = DirectionType.DOWN,
                    resultText = "Вниз"
            ),
            Direction(
                    label = "←",
                    type = DirectionType.LEFT,
                    resultText = "Влево"
            )
    )
}