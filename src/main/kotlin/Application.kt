import javafx.scene.text.FontWeight
import tornadofx.*
import view.MainView

class Application : App(MainView::class, Styles::class)

class Styles : Stylesheet() {
    init {
        label {
            fontSize = 20.px
            fontWeight = FontWeight.BOLD
        }
    }
}