package view

import javafx.scene.control.Button
import javafx.scene.control.ComboBox
import javafx.scene.control.TextField
import javafx.scene.layout.VBox
import model.MainInteractor
import model.WINDOW_HEIGHT_PX
import model.WINDOW_TITLE
import model.WINDOW_WIDTH_PX
import model.models.Direction
import presenter.MainPresenter
import tornadofx.*

class MainView : View() {
    override val root = VBox().apply {
        prefHeight = WINDOW_HEIGHT_PX
        prefWidth = WINDOW_WIDTH_PX
        title = WINDOW_TITLE
    }

    private val mainInteractor = MainInteractor()
    private val presenter = MainPresenter(
            view = this,
            interactor = mainInteractor
    )

    private var startButton by singleAssign<Button>()
    private var resultText by singleAssign<TextField>()
    private var directionsDropdown by singleAssign<ComboBox<Direction>>()


    init {
        resultText = textfield {  }
        startButton = button(text = "Пуск") {
            action { presenter.onStartButtonPressed() }
        }
        directionsDropdown = combobox {
            value = presenter.selectedDirection
            items = presenter.directionsDropdownData
            setOnAction { presenter.onDirectionSelected((it.source as ComboBox<Direction>).value) }
        }


        with(root) {
            this += resultText
            this += startButton
            this += directionsDropdown
        }
    }

    fun setResultText(text: String) {
        resultText.text = text
    }
}