package presenter

import model.MainInteractor
import model.models.Direction
import view.MainView

class MainPresenter(interactor: MainInteractor,
                    private val view: MainView) {

    var directionsDropdownData = interactor.getDirections()
    var selectedDirection = interactor.getDirections().first()

    // View Events
    fun onStartButtonPressed() {
        view.setResultText(selectedDirection.resultText)
    }

    fun onDirectionSelected(direction: Direction) {
        selectedDirection = direction
    }
}